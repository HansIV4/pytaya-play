import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import routes from './router.js'
import { } from './download.js'
import model from "./model.json"


const app = express()
const PORT = 3000



app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());


app.get('/', (req, res) => {
    res.send("Hello");
});

app.get('/jeux', (req, res) => {
    res.send(model)
})


app.get('/jeux/:id', (req, res) => {
    const params = req.params;

    res.send(model[params.id])
})

app.get('/categorie/:categorie', (req, res) => {
    const categorie = req.params.categorie;
    res.send(model.filter(obj => obj.categorie === categorie))

})

app.get('/jeux/:id/comments', (req, res) => {
    const params = req.params;
    res.send(model[params.id].comments)
})

app.put('/jeux/:id/comment', (req, res) => {
    const id = req.params.id;
    const comment = req.body.comment
    const author = req.body.author
    const date = req.body.date

    model[id].comments.push({ "comment": comment, "author": author, "date": date });

    res.send(req.body)

})

app.delete('/jeux/:id/comment', (req, res) => {
    const id = req.params.id;
    const comment = req.body.comment
    const author = req.body.author
    const date = req.body.date
    let index = -1;
    var i = 0;
    for(i = 0; i < model[id].comments.length; i++) {
        if (model[id].comments[i].author == author && model[id].comments[i].comment == comment && model[id].comments[i].date == date) {
            index = i
        }
    }
    if(index == -1){
        res.status(404).send(model[id].comments);
    }else{
        model[id].comments[index] = null;
        model[id].comments = model[id].comments.filter(obj => obj != null)
        res.send(model[id].comments)
    }
})

app.put('/jeux/', (req, res) => {
    const id = model.length;
    const nom = req.body.nom
    const categorie = req.body.categorie
    const auteur = req.body.auteur
    const titre = req.body.titre
    const note = req.body.note
    const review = req.body.Review
    const date = new Date().toUTCString()
    const image = "default.png"

    model.push({
        id: id,
        nom: nom,
        categorie: categorie,
        auteur: auteur,
        titre: titre,
        note: note,
        Review: review,
        date: date,
        image: image,
        comments:[]
     });
    res.send(req.body)
})

app.listen(PORT, () =>
    console.log(`ca roule sur ${PORT}!`));