import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from '@/pages/home/Index.vue';
import News from '@/pages/news/Index.vue';
import WriteReview from '@/pages/writeReview/index.vue'

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/categorie/:categorie',
    name: 'categorie',
    component: Home
  },
  {
    path: '/news/:id',
    name: 'news',
    component: News,
  },
  {
    path:'/writeReview',
    name: 'writeReview',
    component: WriteReview
  }
];

const router = new VueRouter({
  routes,
  mode: 'history',
});

export default router;
